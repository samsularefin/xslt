<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>

<xsl:key name="adjacentByName" match="foo/*" use="generate-id(preceding-sibling::*[not(name()=name(current()))][1])" />

<xsl:template match="/">
<foo><s>
    <xsl:for-each select="foo/*[generate-id()=generate-id(key('adjacentByName', generate-id(preceding-sibling::*[not(name()=name(current()))][1]))[1])]">
        <s>
            <xsl:for-each select="key('adjacentByName', generate-id(preceding-sibling::*[not(name()=name(current()))][1]))">
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </s>
    </xsl:for-each>
</s></foo>
</xsl:template>

</xsl:stylesheet>