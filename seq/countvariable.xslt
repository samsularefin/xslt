<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
  
  <xsl:key name="adjacentByName" match="foo/*" use="generate-id(following-sibling::*[not(name()=name(current()))][1])" />
  
  <xsl:template match="/">
<foo> <s>
<xsl:for-each select="foo/*[generate-id()=generate-id(key('adjacentByName', generate-id(following-sibling::*[not(name()=name(current()))][1]))[1])]">
      
      <xsl:variable name = "mygroup"  select="key('adjacentByName', generate-id(following-sibling::*[not(name()=name(current()))][1]))"/>
      <xsl:choose>
	<xsl:when test="count($mygroup)&gt;1">
 <xsl:for-each select="$myGroup">
<s>            <xsl:copy-of select="."/></s>
	    </xsl:for-each>	  
        </xsl:when>
        <xsl:otherwise>
	  <xsl:copy-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
</xsl:for-each>
</s>
</foo>
    </xsl:template>
    
  </xsl:stylesheet>
  