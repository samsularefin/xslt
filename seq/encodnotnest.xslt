<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:output method="xml" indent="yes"/>
   <xsl:strip-space elements="*"/>

   <xsl:key name="adjacent" match="*/*" use="generate-id(preceding-sibling::*[not(name()=name(current()))][1])" />

   <xsl:template match="/*">
       <xsl:copy>
           <xsl:apply-templates select="comment()"/>
           <s>  
               <xsl:for-each select="*[generate-id()=generate-id(key('adjacent', generate-id(preceding-sibling::*[not(name()=name(current()))][1]))[1])]">
                   <s>
                      <xsl:apply-templates select="key('adjacent', generate-id(preceding-sibling::*[not(name()=name(current()))][1]))"/>
                   </s>
               </xsl:for-each>
           </s>
           <a>
               <xsl:apply-templates select="@*"/>
           </a>
       </xsl:copy>
   </xsl:template>

   <xsl:template match="comment()">
       <c>
           <xsl:value-of select="."/>
       </c>
   </xsl:template>

   <xsl:template match="*">
        <xsl:copy>
            <a>
                <xsl:apply-templates select="@*"/>
            </a>
            <xsl:apply-templates select="text()"/>
        </xsl:copy>
   </xsl:template>

   <xsl:template match="@*">
       <a>
           <a>
               <xsl:value-of select="name()"/>
           </a>
           <v>
               <xsl:value-of select="."/>
           </v>
       </a>
   </xsl:template>

   <xsl:template match="text()">
       <v>
           <xsl:value-of select="."/>
       </v>
   </xsl:template>

</xsl:stylesheet>