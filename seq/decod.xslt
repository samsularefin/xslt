<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 <xsl:output indent="yes"/>
 <xsl:strip-space elements="*"/>
    		    
<xsl:template match="@*|node()">
<xsl:copy><!-- that matches all the nodes and creates a copy of all the nodes unless specified -->
  <xsl:apply-templates select="@*|node()"/>
</xsl:copy>		     
</xsl:template>		     
    
<xsl:template match="*[a]"> <!-- matches all the "a" elements in the xml file-->	
 <xsl:copy>
<xsl:apply-templates select="@*|a/a"/> <!-- applies template rules to all the "a/a" element values -->
<xsl:apply-templates    select="node()[not(self::a)]"/>  <!-- applies template rules to all the elements that are not part of "a" -->               
 </xsl:copy>
</xsl:template>
    
<xsl:template match="a/a" priority="1">
     <xsl:attribute name="{a}"><!-- creates attributes and the name should be the value of "a" -->
      <xsl:value-of select="v"/> <!-- value of the attribute should be the value of "v" -->	  
     </xsl:attribute>
</xsl:template>	
   
<xsl:template match="v"> <!-- removes the v tags in the output -->
     <xsl:apply-templates/>
</xsl:template>   
		      
		      
<xsl:template	match="s"> <!-- removes s tags in the output-->
     <xsl:apply-templates/>
</xsl:template>		   
				   
<xsl:template match="c">   
 <!-- matches c tag and comment out the value of "c" -->
 <xsl:comment>  
<xsl: value-of select="."/>
</xsl:comment>	
</xsl:template>	
</xsl:stylesheet>						