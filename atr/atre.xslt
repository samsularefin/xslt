<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- Matches all of nodes -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select= "*"/>
            <a>
                <xsl:apply-templates select="@*" />
            </a>
            <xsl:apply-templates select="text()" />
        </xsl:copy>
    </xsl:template>

    <!-- Matches all attributes -->
    <xsl:template match="@*">
        <a>
            <a><xsl:value-of select="name()" /></a>
            <v><xsl:value-of select="." /></v>
        </a>
    </xsl:template>

    <!-- Matches text nodes -->
    <xsl:template match="text()">
        <v><xsl:value-of select="." /></v>
    </xsl:template>
</xsl:stylesheet>