<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*[a]">
        <xsl:copy>
            <xsl:apply-templates select="@*|a/a"/>
            <xsl:apply-templates select="node()[not(self::a)]"/>                
        </xsl:copy>
    </xsl:template>

    <xsl:template match="a/a" priority="1">
        <xsl:attribute name="{a}">
            <xsl:value-of select="v"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="v">
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>