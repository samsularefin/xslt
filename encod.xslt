<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:output method="xml" indent="yes"/>
   <xsl:strip-space elements="*"/>
   
  <xsl:key name ="checkgroup" match="*" use="self::*[name() = name(preceding-sibling::*[1])]"/>
   <xsl:key name="groupleader" match="*" use="generate-id(self::*[name() != name(preceding-sibling::*[1])])" />
  <xsl:template match="*[*]">
       <xsl:copy>
           <xsl:apply-templates select="comment()"/>
         <xsl:choose>
           <xsl:when test="(key('checkgroup',*))">
            <s>
        <xsl:for-each select="*[key('groupleader', generate-id())]">
          <s>
            <xsl:apply-templates select=". | following-sibling::*[
              name() = name(current())
              and
              generate-id(current()) = generate-id(
                preceding-sibling::*[key('groupleader', generate-id())][1]
              )
            ]" />
          </s>
        </xsl:for-each>
      </s>
       </xsl:when>
        <xsl:otherwise>
	  <xsl:apply-templates select="*"/>
        </xsl:otherwise>
      </xsl:choose>
         <xsl:if test="@*">
           <a>
               <xsl:apply-templates select="@*"/>
           </a>
         </xsl:if>
       </xsl:copy>
         
   </xsl:template>

   <xsl:template match="comment()">
       <c>
           <xsl:value-of select="."/>
       </c>
   </xsl:template>

   <xsl:template match="*">
        <xsl:copy>
          <xsl:apply-templates select="*"/>
          <xsl:if test="@*">  
          <a> 
                <xsl:apply-templates select="@*"/>
            </a>
          </xsl:if>
            <xsl:apply-templates select="text()"/>
            
        </xsl:copy>
   </xsl:template>

   <xsl:template match="@*">
      
          <a>
           <a>
               <xsl:value-of select="name()"/>
           </a>
           <v>
               <xsl:value-of select="."/>
           </v>
       </a>
   </xsl:template>

   <xsl:template match="text()">
     
     <xsl:choose>
       <xsl:when test="../@*">
       <v>
      <xsl:value-of select="."/>
       </v>
      </xsl:when>
        <xsl:otherwise>
    
            <xsl:value-of select="."/>    
     </xsl:otherwise>
       </xsl:choose>
     
   </xsl:template>

</xsl:stylesheet>