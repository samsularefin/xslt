#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlreader.h>

#include "config.h"
#include "sequence.h"

/* ############# Denormalisation ############# */
void remove_children(xmlNodePtr parent)
{
  xmlNodePtr children = NULL;
  int c, j;
  c = xmlChildElementCount(parent);
  for (j = c - 1; j >= 0; j--){
    children = xmlFirstElementChild(parent);
    xmlUnlinkNode(children);
    xmlFreeNode(children);
  }
}

void restructure_sequence(xmlNodePtr parent)
{
  xmlNodePtr children = NULL, first_seq = NULL, second_seq = NULL, node = NULL;
  xmlChar * cur_element;
  xmlChar * next_element;
  
  //create new structure: first and second sequence
  first_seq = xmlNewNode(NULL, BAD_CAST SEQUENCE_NODE);
  second_seq = xmlNewNode(NULL, BAD_CAST SEQUENCE_NODE); 
  xmlAddChild(first_seq, second_seq); 
  
  for(children = parent->children; children; children = children->next){  
    //if second sequence is null create new one
    if (second_seq == NULL){
      second_seq = xmlNewNode(NULL, BAD_CAST SEQUENCE_NODE); 
      xmlAddChild(first_seq, second_seq); 
    }
    //add all nodes (text included) to second sequence
    node = xmlCopyNode(children, 1);
    xmlAddChild(second_seq, node);
 
    //decide when is time to create new second sequence
    if (children->type == XML_ELEMENT_NODE) {
      cur_element = children->name; 
      if (xmlNextElementSibling(children) != NULL){
	next_element = xmlNextElementSibling(children)->name;
	//if next element is different, null second sequence
	if (xmlStrEqual(cur_element, next_element) == 0){
	  second_seq = NULL;
	}
      }
    }
  }
  //todo: add to generated node
  remove_children(parent);  
  xmlAddChild(parent, first_seq);
  //xmlSaveFormatFileEnc("-", parent->doc, "UTF-8", 1);
}

xmlNodePtr create_node_seq(xmlNodePtr root_node, xmlNodePtr cur_node)
{
  xmlNodePtr new_node = NULL;
  xmlChar * content = NULL;
  xmlChar * ns_elem;
  if(cur_node->ns != NULL){
    ns_elem = xmlStrdup(cur_node->ns->prefix);
    ns_elem = xmlStrcat(ns_elem, (xmlChar *) COLON);
    ns_elem = xmlStrcat(ns_elem, cur_node->name);
    if (xmlChildElementCount(cur_node) > 0 ){
      new_node = xmlNewChild(root_node, NULL,BAD_CAST ns_elem, NULL);
    }else{
      content = xmlNodeGetContent(cur_node->children);
      new_node = xmlNewChild(root_node, NULL, BAD_CAST ns_elem, content);  
      xmlFree(content);
      xmlFree(ns_elem);
    }
  }else{
    if (xmlChildElementCount(cur_node) > 0 ){
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, NULL);
    }else{
      content = xmlNodeGetContent(cur_node->children);
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, content);  
      xmlFree(content);
    } 
  }
  return new_node;
}

void denormalise_sequence_on(xmlNodePtr root_in, xmlNodePtr root_out, int stage)
{
  xmlNode *cur_node = NULL, *new_node = NULL;   
  xmlNode *children = NULL;
  xmlChar *cur_element;
  xmlChar *next_element;
  int count = 0;
  int trigger = 0;
  
  for (cur_node = root_in; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {

      for(children = cur_node->children; children; children = children->next){
      	if (children->type == XML_ELEMENT_NODE){
      	  //set cur_element
      	  cur_element = children->name;	 
	  //	  count++;
      	  //if children is not null
      	  if (xmlNextElementSibling(children) != NULL){
      	    //set next_element
      	    next_element = xmlNextElementSibling(children)->name;
      	    //if cur != next, finish tag
      	    //if is equal to next one then count++
	    if (xmlStrEqual(cur_element, next_element) == 1){
	      count++;
	    }
      	    if (xmlStrEqual(cur_element, next_element) == 0 && count > 0){
      	      //trigger sequence denorm
      	      trigger = 1;
	      count = 0;
	    }
      	  }
      	}
      }
      count = 0;
      if( trigger == 1){	
	restructure_sequence(cur_node);
	trigger = 0;
      }
      new_node = create_node_seq(root_out, cur_node);
      if (xmlChildElementCount(cur_node) > 0 ){
	denormalise_sequence_on(cur_node->children, new_node, 1);
      }
    }
  }
}



xmlDocPtr expand_sequence(xmlDocPtr doc_in)
{
  xmlDocPtr doc_out=NULL;
  xmlNodePtr root_in=NULL, root_out=NULL, gen_node=NULL; 

  root_in = xmlDocGetRootElement(doc_in);
  doc_out = xmlNewDoc(BAD_CAST "1.0");
  root_out = xmlNewNode(NULL, root_in->name);

  denormalise_sequence_on(root_in, root_out, 0);

  gen_node = xmlCopyNodeList(root_out->children);
  xmlFreeNode(root_out);
  xmlDocSetRootElement(doc_out, gen_node);

  return doc_out;
}


/* ############# Normalisation ############# */

void remove_sequence_tags(xmlNodePtr first_child, xmlNodePtr cur_node)
{
  xmlNodePtr cur_child = NULL, seq_child = NULL;
  
  for(cur_child = first_child->children; cur_child; cur_child = cur_child->next){
    if (cur_child->type == XML_ELEMENT_NODE) {
      //printf("cur_child: %s\n", cur_child->name);
      seq_child = xmlCopyNodeList(cur_child->children);
      xmlAddChildList(cur_node, seq_child);
    } 
  }
  xmlUnlinkNode(first_child);
  xmlFreeNode(first_child);
}

void denormalise_sequence_off(xmlNodePtr root_in, xmlNodePtr root_out, int stage)
{
  xmlNodePtr cur_node = NULL, new_node = NULL, first_child=NULL;   
  xmlChar * seq = SEQUENCE_NODE;
  
  for (cur_node = root_in; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      new_node = create_node_seq(root_out, cur_node);
      if (xmlChildElementCount(cur_node) > 0 ){
	//check for sequence structure
	first_child = xmlFirstElementChild(cur_node);
	if(xmlStrcmp(first_child->name, seq) == 0 &&  
	   xmlStrcmp(xmlFirstElementChild(first_child)->name, seq) == 0){
	  //remove sequence from input DOM
	  remove_sequence_tags(first_child, cur_node);
	}
	denormalise_sequence_off(cur_node->children, new_node, 1);
      }
    }
  }
}

xmlDocPtr reduce_sequence(xmlDocPtr doc_in)
{
  xmlDocPtr doc_out=NULL;
  xmlNodePtr root_in=NULL, root_out=NULL, gen_node=NULL; 

  root_in = xmlDocGetRootElement(doc_in);
  doc_out = xmlNewDoc(BAD_CAST "1.0");
  root_out = xmlNewNode(NULL, root_in->name);
  denormalise_sequence_off(root_in, root_out, 0);
  
  gen_node = xmlCopyNodeList(root_out->children);
  xmlFreeNode(root_out);
  xmlDocSetRootElement(doc_out, gen_node);
  
  return doc_out;
}
