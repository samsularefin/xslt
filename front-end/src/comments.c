#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "config.h"
#include "comments.h"

#define ROOT_NAME "root"

/* ############# Denormalisation ############# */

xmlNodePtr create_comment_node(xmlNodePtr root_node, xmlNodePtr cur_node)
{
  xmlNodePtr new_node = NULL;
  xmlChar * content =NULL;
  content = xmlNodeGetContent(cur_node);
  new_node = xmlNewChild(root_node, NULL, BAD_CAST "c", content); 
  xmlFree(content);
  return new_node;
}

xmlNodePtr create_element_node(xmlNodePtr root_node, xmlNodePtr cur_node)
{
  xmlNodePtr new_node = NULL;
  xmlChar * content =NULL;
  if (xmlChildElementCount(cur_node) > 0 ){
    new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, NULL); 
  }else{
    content = xmlNodeGetContent(cur_node);
    new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, content); 
    xmlFree(content);
  }
  return new_node;
}

int check_doc_comments(xmlNodePtr a_node)
{
  xmlNodePtr cur_node = NULL;
  xmlDocPtr doc = NULL;
  int stage = 0;
  //check for doc inside root
  doc = a_node->doc;
  for (cur_node = doc->children; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_COMMENT_NODE) {
      stage = 1;
    }
  }
  return(stage);
}

void denormalise_comments(xmlNodePtr a_node, xmlNodePtr root_node, int stage)
{
  xmlNode *cur_node = NULL;
  xmlNode *new_node = NULL;

  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      new_node = create_element_node(root_node, cur_node);
      if (xmlChildElementCount(cur_node) > 0 ){
	denormalise_comments(cur_node->children, new_node, 1);
      }
    }
    else if (cur_node->type == XML_COMMENT_NODE) {
      new_node = create_comment_node(root_node, cur_node);
      if (xmlChildElementCount(cur_node) > 0 ){
	denormalise_comments(cur_node->children, new_node, 1);
      }
    }
  }
}


/* ############# Normalisation ############# */

int normalise_comments(xmlDocPtr doc) {
    xmlXPathContextPtr xpathCtx; 
    xmlXPathObjectPtr xpathObj; 
    const xmlChar* xpathExpr = "//c";
    assert(xpathExpr);

    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(doc); 
        return(-1);
    }
    
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    if(xpathObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
        xmlXPathFreeContext(xpathCtx); 
        xmlFreeDoc(doc); 
        return(-1);
    }

    if (xmlStrcmp (xmlDocGetRootElement(doc)->name, (xmlChar*) ROOT_NAME) == 0){
      norm_xpath_comments_nodes(xpathObj->nodesetval, 1);
    }else{
      norm_xpath_comments_nodes(xpathObj->nodesetval, 0);
    }
  

    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx); 

    return(0);
}

void norm_xpath_comments_nodes(xmlNodeSetPtr nodes, int stage) {
    
    int size;
    int i,c,j;
    xmlNodePtr comment_node=NULL, root_node=NULL;
    xmlChar* comment_content;
    size = (nodes) ? nodes->nodeNr : 0;
    
    if(stage == 0 )
      {
	for(i = size - 1; i >= 0; i--) {
	  assert(nodes->nodeTab[i]);
	  comment_content = xmlNodeGetContent(nodes->nodeTab[i]);
	  comment_node = xmlNewComment(comment_content);
	  xmlFree(comment_content);
	  xmlAddPrevSibling(nodes->nodeTab[i], comment_node);
	  xmlUnlinkNode(nodes->nodeTab[i]); 
	  xmlFreeNode(nodes->nodeTab[i]);
	  
	  if (nodes->nodeTab[i]->type != XML_NAMESPACE_DECL)
	    nodes->nodeTab[i] = NULL;
	}
      }else{
 
      root_node = xmlDocGetRootElement(nodes->nodeTab[0]->doc);
      xmlDocSetRootElement(nodes->nodeTab[0]->doc, xmlFirstElementChild(root_node));
      
      c = xmlChildElementCount(root_node);
      for (j = c - 1; j >= 0; j--){
	//printf("NTR: %s\n", xmlFirstElementChild(root_node)->name);
	xmlAddSibling(xmlDocGetRootElement(nodes->nodeTab[0]->doc), xmlFirstElementChild(root_node)); 
      }
      
      for(i = size - 1; i >= 0; i--) {
	assert(nodes->nodeTab[i]);
	comment_content = xmlNodeGetContent(nodes->nodeTab[i]);
	comment_node = xmlNewComment(comment_content);
	xmlFree(comment_content);
	xmlAddPrevSibling(nodes->nodeTab[i], comment_node);
	xmlUnlinkNode(nodes->nodeTab[i]); 
	xmlFreeNode(nodes->nodeTab[i]);
	
	if (nodes->nodeTab[i]->type != XML_NAMESPACE_DECL)
	  nodes->nodeTab[i] = NULL;
      } 
    }
}


