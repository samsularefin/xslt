#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "config.h"
#include "attributes.h"

 /* ############# Denormalisation ############# */

int stage =  0;

void transform_single_tag(xmlNodePtr a_node, xmlNodePtr root_node, int tree_option)
{
  xmlNodePtr attr= NULL,  attr_list=NULL, v_node=NULL;
  xmlAttr *attr_node =NULL;
  xmlNsPtr cur_elem_ns_def = NULL;
  int i = 0;
  xmlChar * ns_def;
  xmlChar * ns_attr;
  xmlChar* val_v_content;
  xmlChar* val_content;
  xmlChar* val_string;

  //check first for nsDef and then for attributes
  //NB: need to fix ordering. TODO join for loops
 
  for(cur_elem_ns_def = a_node->nsDef; NULL != cur_elem_ns_def; cur_elem_ns_def = cur_elem_ns_def->next){
    if (i == 0){
      attr_list = xmlNewNode(NULL, BAD_CAST ATTRIBUTE_NODE);
    }
    attr = xmlNewChild(attr_list, NULL, BAD_CAST  ATTRIBUTE_NODE, NULL);
    if (cur_elem_ns_def->prefix != NULL){
      ns_def = xmlStrdup((xmlChar *) NS_DEF);
      ns_def = xmlStrcat(ns_def, (xmlChar *) COLON);
      ns_def = xmlStrcat(ns_def, cur_elem_ns_def->prefix );
    }else{
      ns_def = xmlStrdup((xmlChar *) NS_DEF);
    }
    xmlNewChild(attr,NULL,BAD_CAST  ATTRIBUTE_NODE, BAD_CAST ns_def);
    xmlNewChild(attr,NULL,BAD_CAST ATTRIBUTE_VAL_NODE, BAD_CAST cur_elem_ns_def->href);
    i++;
    xmlAddChild(a_node, attr_list);
    xmlFree(ns_def);
  }
  
  for(attr_node = a_node->properties; NULL != attr_node; attr_node = attr_node->next){
    if (i == 0){
      attr_list = xmlNewNode(NULL, BAD_CAST  ATTRIBUTE_NODE);
    }
    val_v_content = xmlGetProp(a_node, attr_node->name);
    if (attr_node->ns != NULL){
      attr = xmlNewChild(attr_list, NULL, BAD_CAST  ATTRIBUTE_NODE, NULL);
      ns_attr =  xmlStrdup(attr_node->ns->prefix);
      ns_attr = xmlStrcat(ns_attr, (xmlChar *) COLON);
      ns_attr = xmlStrcat(ns_attr, attr_node->name);
      xmlNewChild(attr,NULL, BAD_CAST  ATTRIBUTE_NODE, BAD_CAST ns_attr);
      xmlNewChild(attr,NULL, BAD_CAST ATTRIBUTE_VAL_NODE, val_v_content);
      i++;
      xmlAddChild(a_node, attr_list);   
      xmlFree(val_v_content);
      xmlFree(ns_attr);
    }else{   
      attr = xmlNewChild(attr_list, NULL, BAD_CAST ATTRIBUTE_NODE, NULL);
      xmlNewChild(attr,NULL, BAD_CAST  ATTRIBUTE_NODE, attr_node->name);
      xmlNewChild(attr,NULL, BAD_CAST ATTRIBUTE_VAL_NODE, val_v_content);
      i++;
      xmlAddChild(a_node, attr_list);
      xmlFree(val_v_content);
    }
  }
  if (tree_option == 1)
    {
      if(i > 0){
	val_string = xmlNodeListGetString(a_node->doc, a_node->xmlChildrenNode, 1);
	v_node = xmlNewNode(NULL, BAD_CAST ATTRIBUTE_VAL_NODE);
	xmlNodeAddContent(v_node, val_string);
	xmlAddNextSibling(attr_list, v_node);
	xmlFree(val_string);
      }else{
	val_content = xmlNodeGetContent(a_node);
	xmlNodeSetContent(root_node, val_content);
	xmlFree(val_content);
      }
    }  
}


xmlNodePtr create_node(xmlNodePtr root_node, xmlNodePtr cur_node, int stage)
{
  xmlNodePtr new_node = NULL;
  xmlChar * ns_elem;
  //ns parsed as element name
  if(cur_node->ns != NULL){
    if (cur_node->ns->prefix == NULL && cur_node->ns->href != NULL){
      ns_elem = xmlStrdup(cur_node->name);
    }else{
      ns_elem = xmlStrdup(cur_node->ns->prefix);
      ns_elem = xmlStrcat(ns_elem, (xmlChar *) COLON);
      ns_elem = xmlStrcat(ns_elem, cur_node->name);
    }
    if (xmlChildElementCount(cur_node) > 0 ){
      new_node = xmlNewChild(root_node, NULL,BAD_CAST ns_elem, NULL);
      transform_single_tag(cur_node, new_node, 0);
    }else{
      new_node = xmlNewChild(root_node, NULL, BAD_CAST ns_elem, NULL);  
      transform_single_tag(cur_node, new_node, 1);
    }
    xmlFree(ns_elem);
  }else{ 
    if (xmlChildElementCount(cur_node) > 0 ){
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, NULL);
      transform_single_tag(cur_node, new_node, 0);
    }else{
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, NULL);  
      transform_single_tag(cur_node, new_node, 1);
    }
  }
  return new_node;
}

void parse_and_create(xmlNodePtr root_in, xmlNodePtr root_out, int stage)
{
  xmlNode *cur_node = NULL, *new_node = NULL;
   
  for (cur_node = root_in; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      new_node = create_node(root_out, cur_node, stage);
      if (xmlChildElementCount(cur_node) > 0 ){
	parse_and_create(cur_node->children, new_node, 1);
      }
    }
  }
}

xmlDocPtr expand_attributes(xmlDocPtr doc_in)
{
  xmlDocPtr doc_out=NULL;
  xmlNodePtr root_in=NULL, root_out=NULL, gen_node=NULL; 

  root_in = xmlDocGetRootElement(doc_in);
  //create new DOM
  doc_out = xmlNewDoc(BAD_CAST "1.0");
  root_out = xmlNewNode(NULL, root_in->name);
  parse_and_create(root_in, root_out, 0); 
  gen_node = xmlCopyNodeList(root_out->children);
  xmlFreeNode(root_out);
  xmlDocSetRootElement(doc_out, gen_node);

  return doc_out;
}



 /* ############# Normalisation ############# */



int check_attributes_node(xmlNodePtr cur_node)
{
  xmlNodePtr atr_node = NULL, atr_child = NULL;
  if(xmlStrEqual(cur_node->name, BAD_CAST ATTRIBUTE_NODE) == 1){
    if (xmlChildElementCount(cur_node) > 0 ){
      atr_node = xmlFirstElementChild(cur_node);
      if(xmlStrEqual(atr_node->name, BAD_CAST ATTRIBUTE_NODE) == 1){
	if (xmlChildElementCount(atr_node) > 0 ){
	  atr_child = xmlFirstElementChild(atr_node);
	  if(xmlStrEqual(atr_child->name, BAD_CAST ATTRIBUTE_NODE) == 1){
	    //    printf("FOUND attribute\n");
	    return 1;
	  }
	}
      }
    }
  }
  return 0;
}

xmlNodePtr create_element_node_atr(xmlNodePtr root_node, xmlNodePtr cur_node, int bool)
{
  xmlNodePtr new_node = NULL;
  xmlNodePtr atr_node = NULL, atr_a = NULL, atr_v = NULL, v_node = NULL; 
  xmlChar * content =NULL,  * a_content = NULL,* v_content = NULL, * value_content = NULL;
  if(bool == 1){
    // printf("cur_node: %s\n",cur_node->name);
    for (atr_node = cur_node->children; atr_node; atr_node = atr_node->next) {
      if (atr_node->type == XML_ELEMENT_NODE) {
	//	printf("atr_node: %s\n",atr_node->name);
	atr_a = xmlFirstElementChild(atr_node);
	atr_v = xmlNextElementSibling(atr_a);
	a_content =  xmlNodeGetContent(atr_a);
	v_content =  xmlNodeGetContent(atr_v);
	xmlNewProp(root_node, a_content , v_content);
	xmlFree(a_content);
	xmlFree(v_content);
      }
    }
    if( xmlNextElementSibling(cur_node) != NULL){
      v_node = xmlNextElementSibling(cur_node);
      value_content = xmlNodeGetContent(v_node);
      xmlNodeSetContent(root_node, value_content); 
      xmlFree(value_content);
    }
    
  }else{    
    if (xmlChildElementCount(cur_node) > 0 ){
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, NULL);
    }else{
      content = xmlNodeGetContent(cur_node);
      new_node = xmlNewChild(root_node, NULL, BAD_CAST cur_node->name, content);
      xmlFree(content);
    }
  }
  return new_node;
}


void parse_attributes(xmlNodePtr a_node, xmlNodePtr root_node, int stage)
{
  xmlNode *cur_node = NULL;
  xmlNode *new_node = NULL;
  int bool = 0;
 
  for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
    if (cur_node->type == XML_ELEMENT_NODE) {
      //printf("PARSE: cur_node %s\n", cur_node->name);
      bool = check_attributes_node(cur_node);	
      new_node = create_element_node_atr(root_node, cur_node, bool);
      
      if(bool == 1){
	if(xmlNextElementSibling(cur_node) != NULL){
	  if(xmlStrEqual(xmlNextElementSibling(cur_node)->name, BAD_CAST ATTRIBUTE_VAL_NODE) == 1){
	    cur_node = xmlNextElementSibling(cur_node);
	  }
	}
      }
      if (xmlChildElementCount(cur_node) > 0 ){
	parse_attributes(cur_node->children, new_node, 0);
	
      }
    }
  }
}

xmlDocPtr reduce_attributes(xmlDocPtr doc_in)
{
  xmlDocPtr doc_out=NULL;
  xmlNodePtr root_in=NULL, root_out=NULL, gen_node=NULL;

  root_in = xmlDocGetRootElement(doc_in);
  doc_out = xmlNewDoc(BAD_CAST "1.0");
  root_out = xmlNewNode(NULL, root_in->name);
  //parse attributes
  parse_attributes(root_in, root_out, 0); 
  gen_node = xmlCopyNodeList(root_out->children);
  xmlFreeNode(root_out);
  xmlDocSetRootElement(doc_out, gen_node);

  return doc_out;
}
