#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "config.h"
#include "attributes.h"
#include "sequence.h"

int main(int argc, char **argv)
{

  xmlDocPtr doc_out=NULL, doc_in=NULL, doc_denorm=NULL;
  //xmlNodePtr root_in=NULL, root_out=NULL, gen_node=NULL; 
  
  /* Parse command line and process file */
  if (argc != 2) {
    fprintf(stderr, "Error: wrong number of arguments.\n");
    // usage(argv[0]);
    return(-1);
  } 
  /* Init libxml */     
  xmlInitParser();
  
  doc_in = xmlReadFile(argv[1], NULL, XML_PARSE_NONET|XML_PARSE_RECOVER|XML_PARSE_NOWARNING|XML_PARSE_NOERROR);
  if (doc_in == NULL) {
    printf("error: could not parse file %s\n", argv[1]);
  }

  /* Denormalise attribute nodes */
  doc_denorm = expand_attributes(doc_in);
  if(doc_denorm == NULL) {
    printf("error: could not denormalise doc\n");
  }
  
  /* Denormalise sequence nodes */
  doc_out =  expand_sequence(doc_denorm);
  if (doc_out == NULL) {
    printf("error: could not transform sequence\n");
  }

  //Dumping document to stdio or file
  xmlSaveFormatFileEnc("-", doc_out, "UTF-8", 1);

  //what happen to all the docs? 
  xmlFreeDoc(doc_in);
  xmlFreeDoc(doc_out);
  xmlFreeDoc(doc_denorm);
  xmlCleanupParser();
  xmlMemoryDump();
 
  return EXIT_SUCCESS;
  
}
