#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

/* ############# Denormalisation ############# */
void transform_single_tag(xmlNodePtr a_node, xmlNodePtr root_node, int tree_option);
xmlNodePtr create_node(xmlNodePtr root_node, xmlNodePtr cur_node, int stage);
void parse_and_create(xmlNodePtr root_in, xmlNodePtr root_out, int stage);
xmlDocPtr expand_attributes(xmlDocPtr doc_in);

/* ############# Normalisation ############# */
int check_attributes_node(xmlNodePtr cur_node);
xmlNodePtr create_element_node_atr(xmlNodePtr root_node, xmlNodePtr cur_node, int bool);
void parse_attributes(xmlNodePtr a_node, xmlNodePtr root_node, int stage);
xmlDocPtr reduce_attributes(xmlDocPtr doc_in);

