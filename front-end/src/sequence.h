#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlreader.h>

/* ############# Denormalisation ############# */
void remove_children(xmlNodePtr parent);
void restructure_sequence(xmlNodePtr parent);
xmlNodePtr create_node_seq(xmlNodePtr root_node, xmlNodePtr cur_node);
void denormalise_sequence_on(xmlNodePtr root_in, xmlNodePtr root_out, int stage);
xmlDocPtr expand_sequence(xmlDocPtr doc_in);

/* ############# Normalisation ############# */
void remove_sequence_tags(xmlNodePtr first_child, xmlNodePtr cur_node);
void denormalise_sequence_off(xmlNodePtr root_in, xmlNodePtr root_out, int stage);
xmlDocPtr reduce_sequence(xmlDocPtr doc_in);
