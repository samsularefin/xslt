#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

xmlNodePtr create_comment_node(xmlNodePtr root_node, xmlNodePtr cur_node);
xmlNodePtr create_element_node(xmlNodePtr root_node, xmlNodePtr cur_node);
int check_doc_comments(xmlNodePtr a_node);
void denormalise_comments(xmlNodePtr a_node, xmlNodePtr root_node, int stage);

int normalise_comments(xmlDocPtr doc);
void norm_xpath_comments_nodes(xmlNodeSetPtr nodes, int stage);
