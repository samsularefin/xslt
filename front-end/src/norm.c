#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "config.h"
#include "attributes.h"
#include "sequence.h"

int main(int argc, char **argv)
{
  xmlDocPtr doc;
  xmlDocPtr doc_out=NULL, doc_parsed=NULL;
  
  /* Load XML document */
  doc = xmlReadFile(argv[1], NULL, XML_PARSE_NONET|XML_PARSE_RECOVER|XML_PARSE_NOWARNING|XML_PARSE_NOERROR);
  if (doc == NULL) {
    fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[1]);
    return(-1);
  }
  /* Parse command line and process file */
  if (argc != 2) {
    fprintf(stderr, "Error: wrong number of arguments.\n");
    // usage(argv[0]);
    return(-1);
  } 
  /* Init libxml */     
  xmlInitParser();
  
  doc_out = reduce_sequence(doc);
  if (doc_out == NULL) {
    printf("error: could not transform sequence\n");
  }

  doc_parsed = reduce_attributes(doc_out);
  if (doc_parsed == NULL) {
    printf("error: could not transform attributes\n");
  }

  xmlSaveFormatFileEnc("-", doc_parsed, "UTF-8", 1);  

  xmlFreeDoc(doc);
  xmlFreeDoc(doc_parsed);
  xmlFreeDoc(doc_out);
  xmlCleanupParser();
  xmlMemoryDump();

  return EXIT_SUCCESS;
  
}
