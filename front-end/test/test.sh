#!/bin/bash
echo "~~~~~~~~  XML Transformation Test  ~~~~~~~~"

clear()
{
    rm $1*.out.xml
    rm $1*.new.xml
}

differ()
{
    firstFile=$1
    secondFile=$2
    diff --ignore-all-space --ignore-blank-lines  -y --suppress-common-lines $firstFile $secondFile;
    # --ignore-all-space --ignore-blank-lines
} 

transform()
{
    if [ $# -eq 0 ]
    then
	for file in ../corpus/*
	do
	    if
		[ ${file: -4} == ".xml" ]
	    then
		number=${file: 0:-4}
		echo "Tranforming $file"
		../src/denorm $number.xml > $number.out.xml
		../src/norm $number.out.xml > $number.new.xml
		differ $number.xml $number.new.xml
		#echo ""
		clear ../corpus/
	    fi
	done
    elif [ $# -eq 1 ]
    then 
	for file in $1*

	do
	    if
		[ ${file: -4} == ".xml" ]
	    then
		number=${file: 0:-4}
		echo "Tranforming $file"
		../src/denorm $number.xml > $number.out.xml
		../src/norm $number.out.xml > $number.new.xml
		differ $number.xml $number.new.xml
	#	echo ""
		clear $1
	    fi
	done
    fi
    
}

if [ $# -eq 0 ]
then
    transform
elif [ $# -eq 1 ]
then
    transform $1
fi